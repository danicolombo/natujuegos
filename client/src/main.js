import App from './App.vue'
import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
//IMPORTAR LOS ELEMENTOS A USAR NO TODA LA LIBRERÍA
//import BootstrapVue from 'bootstrap-vue'
import { CardPlugin } from 'bootstrap-vue'
import { ButtonPlugin } from 'bootstrap-vue'
import { FormPlugin } from 'bootstrap-vue'
import { ModalPlugin } from 'bootstrap-vue'
import { NavPlugin } from 'bootstrap-vue'
import { ListGroupPlugin } from 'bootstrap-vue'
import { JumbotronPlugin } from 'bootstrap-vue'



//import 'bootstrap/dist/css/bootstrap.css'
//import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueResource from "vue-resource"
import router from "./router"
import 'es6-promise/auto'
import store from './store'

Vue.use(ButtonPlugin)
Vue.use(CardPlugin)
Vue.use(FormPlugin)
Vue.use(ModalPlugin)
Vue.use(NavPlugin)
Vue.use(ListGroupPlugin)
Vue.use(JumbotronPlugin)


Vue.config.productionTip = false





new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
