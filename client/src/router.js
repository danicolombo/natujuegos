import Vue from "vue";
import Router from "vue-router";

import Juego from "./views/Juego.vue"
import JuegoAvanzado from "./views/JuegoAvanzado.vue"
import Home from "./views/Home.vue"

Vue.use(Router)

export default new Router({
  scrollBehavior() {
    return {x:0, y:0}
  },
  
  linkExactActiveClass: "active",
  mode: "history",
  routes:[
    {
      path: '/juego',
      components: {
        default: Juego
      }},
    {
      path: '/juegoavanzado',
      components: {
        default: JuegoAvanzado
      }},
    {
      path: '/',
      components: {
        default: Home
      }}
  ]
})
