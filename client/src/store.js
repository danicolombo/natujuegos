import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    eco: '',
    rarity: '1'
  },
  mutations: {
    ELEGIR_ECORREGION: (state, payload) => {
      state.eco = payload
    },
    ELEGIR_RAREZA: (state, payload) => {
      state.rarity = payload
    }
  },

  getters: {
    getEcorregion: state => {
      return state.eco
    },
    getRarity: state => {
      return state.rarity
    },

  },
  actions: {
    //toggle: (context) {
    //  context.comit('')
    }
  }
)
