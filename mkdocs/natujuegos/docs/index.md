# Bienvenidxs a Natujuegos

Para visitar el sitio [natujuegos](http://natujuegos.herokuapp.com/).

## Acerca del proyecto

Natujuegos es una herramienta digital que transforma el aprendizaje en una actividad colaborativa y divertida.

En esta primera entrega desarrollamos un juego para aprender a identificar las aves de Argentina, pudiendo elegir entre las distintas ecorregiones del país y varios niveles de dificultad, con la espectativa de incorporar en el futuro a las demás epecies de flora y fauna.

Si te interesa participar o ayudar de algún modo no dudes en escribirnos, el contacto está al final de la página. 

## Directorios

    client/			# Frontend.
    mkdocs/natujuegos/
        mkdocs.yml		# Homepage de la documentacion.
        docs/index.md		# Other markdown pages, images and other files.
    server/			# Backend.
    node_modules/		# Modulos
    models/			# Model para moongose.

## Requerimientos
* `VueJS` - Framework de Javascript desarrollado en base a React y Angular
* `NodeJS` - Entorno en tiempo de ejecución multiplataforma
* `MongoDB` - Sistema de base de datos NoSQL orientado a documentos de código abierto
* `Mongoose` - Base de datos que almacena sus datos como documentos
* `Bootstrap` - Biblioteca multiplataforma o conjunto de herramientas de código abierto para diseño de sitios y aplicaciones web

## Contribuciones
Contactate por email: natujuegos@protonmail.com

## Licencia 
Esta obra está bajo una Licencia Creative Commons Atribución-NoComercial 4.0 Internacional. 
