const Joi = require('@hapi/joi');
const express = require('express');
const mongoose = require('mongoose')

// Instanciamos express
const app = express();


// MIDDLEWARE para procesar respuesta json
app.use(express.json());


data = require('./userandpassword.json')
userandpassword = data.data

// Conección a DB atlas
mongoose.connect(`${userandpassword}@cluster0-64td1.mongodb.net/natujuegos`, {useNewUrlParser: true})
const db = mongoose.connection
db.on('error', (error)=> console.error(error))
db.once('open', (error)=> console.log('Connected to Database'))


// Rutas de la API
//Ecorregion
const ecorregiones = require('./routes/api/ecorregiones');
//derivamos los llamados POST a la API, al archivo ecorregion.js
app.use('/api/ecorregiones', ecorregiones);

// Baires city birds
const baires_city_birds = require('./routes/api/baires_city_birds');
// derivamos los llamados POST a la API, al archivo baires_city_birds.js
app.use('/api/baires_city_birds', baires_city_birds);


// Servir imágenes
app.use(express.static('public'))
app.use(express.static('images'))
app.use(express.static('birds'))


//PORT (puerto virtual para deploy o puerto 3000 si no está disponible)
// activar PORT desde consola con $export PORT=5000
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening on port ${port}`))



// Rutas de la API
// importamos el archivo posts
//const posts = require('./routes/api/posts');
//derivamos los llamados POST a la API, al archivo posts
//app.use('/api/posts', posts);




// importamos el archivo ecorregiones



// Chequeo para activar opciones de PRODUCCIÓN
if(process.env.NODE_ENV === 'production') {
  // Static folder
  app.use(express.static(__dirname + '/public/'));

  // Single Page Application
  // la expresión entre barras se refiere a "cualquier ruta", debe declararse
  // debajo de las variables como const posts = require('./routes/api/posts');
  app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'));

}
