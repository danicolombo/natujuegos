const express = require('express');
const _ = require('lodash');
const Ecorregion = require('../../../models/ecorregion')

const router = express.Router();

// loadBirdCollection
//async function loadBirdCollection() {
//  const client = await mongodb.MongoClient.connect
//  ('mongodb+srv://mauro:merliniox22@cluster0-64td1.mongodb.net/natujuegos', {
//    useNewUrlParser: true, useUnifiedTopology: true
//  });
//  return client.db('natujuegos').collection('aves')
//}


// Get posts (la '/ no refiere al local-host/home sino a lo que se haya derivado')
// Por ejemplo en este caso deriva a './routes/api/baires_city_birds'
router.get('/', async (req, res) => {

  // entre las llaves se pueden hacer querys
  const birds_pack = await Ecorregion.find({"most_seen":1})

  let question_pack = createQuestionPack(birds_pack)

  //console.log(question_pack["results"][0].incorrect_answers)
  res.send(question_pack);
});

// función que arma los paquetes de n aves para preguntas y devuelve n
// preguntas con sus respuestas en un json
function createQuestionPack (input_pack) {
  let input_pack_copy = input_pack
  // question_pack = [];
  let question_pack_template = {
    "response_code":0,
    "results": []
    };
  for (var i = 1; i < 6; i++) {
    let raw_question = loadData(input_pack_copy)
    question_pack_template.results.push(raw_question)

    //BORRAR DE INPUT PACK COPY EL AVE QUE YA SALIÓ
    input_pack_copy = input_pack_copy.filter(function( obj ) {
    return obj.name !== raw_question.correct_answer;
});

  }

  return question_pack_template

};

function loadData (input_pack) {
  let question_template =  {
    "category":"",
    "type":"",
    "difficulty":"",
    "photos":[],
    "correct_answer": "",
    "incorrect_answers":"",
    "author": ""

  }

  let question = question_template;
  let animalSelection = _.sampleSize(input_pack, [n=5]);
  let answer = _.sampleSize(animalSelection, [n=1])[0];

  question["categoty"] = "animals";
  question["correct_answer"] = answer.name;
  question["incorrect_answers"] = animalSelection
                                    .map(a => a.name)
                                    .filter(animalSelection =>
                                      animalSelection !== question["correct_answer"])
  question["author"] = answer.urlOriginal

  // photos
  // INTENTAR CON COMANDO __DIRNAME o VER SI FUNCIONA ASÍ
  if(process.env.NODE_ENV === 'production'){
    question["photos"] = answer.photo.replace('/img/','/images/');
  }else {
    question["photos"] = answer.photo.replace('/img/','http://localhost:5000/images/');
  }
  return question
  }







module.exports = router;
