const express = require('express');
const _ = require('lodash');

const router = express.Router();
const Ecorregion = require('../../../models/ecorregion')

// Get ECORREGION
router.get('/:regions', async (req, res) => {
  try {
    let selected = req.params.regions
    const birds_pack = await Ecorregion.find({$or:[{regions:selected}, {regions:'todo_el_pais'}], rarity:5 })
    console.log(birds_pack.length)
    let question_pack = createQuestionPack(birds_pack)
    res.json(question_pack)
  } catch (err) {
      res.status(500).json( {messaje: err.message})
  }
})





//  const birds_pack = await birds.find({ regions : { $eq : "ecorregion" } }).toArray();
//  console.log(birds_pack)

////
//  res.send(question_pack);

  //////console.log(question_pack["results"][0].incorrect_answers)

  //res.send(${req.params.name})
//});



// función que arma los paquetes de n aves para preguntas y devuelve n
// preguntas con sus respuestas en un json
function createQuestionPack (input_pack) {
  let input_pack_copy = input_pack
  // question_pack = [];
  let question_pack_template = {
    "response_code":0,
    "results": []
    };
  for (var i = 1; i < 6; i++) {
    let raw_question = loadData(input_pack_copy)
    question_pack_template.results.push(raw_question)

    //BORRAR DE INPUT PACK COPY EL AVE QUE YA SALIÓ
    input_pack_copy = input_pack_copy.filter(function( obj ) {
    return obj.name !== raw_question.correct_answer;
});

  }

  return question_pack_template

};

function loadData (input_pack) {
  let question_template =  {
    "category":"",
    "type":"",
    "difficulty":"",
    "photos":[],
    "correct_answer": "",
    "incorrect_answers":""
  }

  let question = question_template;
  let animalSelection = _.sampleSize(input_pack, [n=5]);
  let answer = _.sampleSize(animalSelection, [n=1])[0];

  question["categoty"] = "animals";
  question["correct_answer"] = answer.name;
  question["incorrect_answers"] = animalSelection
                                    .map(a => a.name)
                                    .filter(animalSelection =>
                                      animalSelection !== question["correct_answer"])


  // photos
    if(process.env.NODE_ENV === 'production'){
    if (answer.photo == '') {
      question["photos"] = answer.photo.replace('','gavilanMixto.jpg');
    }else if (answer.photo !== '') {
      question["photos"] = answer.photo.replace('/img/','/images/');

    }
  }else {
    if (answer.photo != null) {
      question["photos"] = answer.photo.replace('/img/','http://localhost:5000/images/');
    }

  return question
  }
}






module.exports = router;
